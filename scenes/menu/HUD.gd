extends CanvasLayer
@onready var life_bar = $LifeBar
@onready var anger_bar = $AngerBar
@onready var indicator = $Indicator
@onready var animation_player = $AnimationPlayer

var death = load("res://assets/sprites/indicator-near-death.png")
var neutral = load("res://assets/sprites/indicator-near-neutral.png")
var zoomie = load("res://assets/sprites/indicator-near-zooming.png")

func init():
	life_bar.value = 0
	anger_bar.value = 0
	indicator.position.x = ((50 * life_bar.size.x) / 100) + life_bar.position.x
	indicator.texture = neutral
	indicator.modulate = Color8(255,255,255)
	animation_player.play("init_bars")

func changeIndicator():
	if life_bar.value < 33:
		indicator.texture = death
	elif life_bar.value < 66:
		indicator.texture = neutral
	elif life_bar.value < 100:
		indicator.texture = zoomie

	indicator.modulate = Color8(255,255,255)
	var tween = get_tree().create_tween()
	if life_bar.value > 50:
		var red = 255 - ((life_bar.value * 255)/100)
		tween.tween_property(indicator, "modulate", Color8(255,int(red),int(red)), 0.5)
	if life_bar.value <= 50:
		var gray = 30 + ((life_bar.value * 255)/100)
		tween.tween_property(indicator, "modulate", Color8(int(gray),int(gray),int(gray)), 0.5)

func moveIndicatorLeft():
	return Vector2(((life_bar.value * life_bar.size.x) / 100) + life_bar.position.x + (indicator.get_rect().size.x/2), indicator.position.y)

func moveIndicatorRight():
	return Vector2(((life_bar.value * life_bar.size.x) / 100) + life_bar.position.x - (indicator.get_rect().size.x/2), indicator.position.y)
	
func increase(value: float):
	if life_bar.value >= 100:
		return

	var tween = get_tree().create_tween()
	tween.set_parallel()
	tween.tween_property(life_bar, "value", life_bar.value + value, 0.5)
	tween.tween_property(anger_bar, "value", anger_bar.value - value, 0.5)
	tween.tween_property(indicator, "position", moveIndicatorLeft(), 0.5)
	changeIndicator()

func decrease(value: float):
	if life_bar.value <= 0:
		return

	var tween = get_tree().create_tween()
	tween.set_parallel()
	tween.tween_property(life_bar, "value", life_bar.value - value, 0.5)
	tween.tween_property(anger_bar, "value", anger_bar.value + value, 0.5)
	tween.tween_property(indicator, "position", moveIndicatorRight(), 0.5)
	changeIndicator()

func get_value():
	return life_bar.value

extends CanvasLayer
@onready var play = $Play
@onready var player_name = $PlayerName
signal new_game_requested

func _on_player_name_text_changed(new_text):
	play.disabled = new_text == ""

func _on_play_pressed():
	emit_signal("new_game_requested", player_name.text)

extends CharacterBody2D
signal killed
signal bit
signal targeted
@onready var animation_player = $AnimationPlayer

var life = 5
var can_attack = false

func hurt():
	life -= 1
	animation_player.play("hurt")
	if life == 0:
		emit_signal("killed")
		queue_free()

func _on_attack_timer_timeout():
	if can_attack:
		animation_player.play("attack")
		emit_signal("bit", 30)


func _on_selection_zone_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("click"):
		emit_signal("targeted", self)

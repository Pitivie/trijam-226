extends Node2D

@onready var slash_sound = $Slash

var attack_left = false

func attack():
	slash_sound.play()
	if attack_left:
		$AnimationPlayer.play("attack_left")
	else:
		$AnimationPlayer.play("attack_right")

func _on_area_sword_body_entered(body):
	if body.is_in_group("enemies") && $AnimationPlayer.is_playing():
		body.hurt()

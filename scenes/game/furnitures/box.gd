extends Node2D
@onready var cat_name = $CatName
@onready var animation_player = $AnimationPlayer
@onready var boss = $Boss
signal reached
signal boss_entered
signal boss_arrived
signal boss_defeated
var is_boss_released = false
var is_boss_owned = false

func setBoxName(box_name: String):
	print(box_name)
	cat_name.text = box_name

func _on_interact_body_entered(body):
	if body.name == "Cat" and is_boss_owned:
		emit_signal("reached")
		queue_free()

func _on_trigger_boss_body_entered(body):
	if !is_boss_released and body.name == "Cat":
		animation_player.play("appear")
		is_boss_released = true
		emit_signal("boss_entered")

func _on_animation_player_animation_finished(anim_name):
	emit_signal("boss_arrived")


func _on_boss_killed():
	is_boss_owned = true
	emit_signal("boss_defeated")

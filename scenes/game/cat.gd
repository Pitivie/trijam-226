extends CharacterBody2D

@onready var cat_sprite = $CatShape/AnimatedSprite2D
@onready var hurtbox_shape = $Hurtbox/CollisionShape2D
@onready var sword = $Sword
@onready var sword_sprite = $Sword/SwordSprite
@onready var sword_shape = $Sword/AreaSword/SwordCollisionShape

signal hurt
const speed = 300
var target_vector
var goes_left = false
var can_move = false

func _ready():
	reset()

func reset():
	target_vector = position
	show()
	can_move = true

func kill():
	hide()
	can_move = false

func _input(event):
	if event.is_action_pressed("click") && can_move:
		target_vector = get_global_mouse_position()

func _physics_process(_delta):
	velocity = position.direction_to(target_vector) * speed
	if position.distance_to(target_vector) > 10:
		move_and_slide()
		cat_sprite.play("walk")
	else:
		cat_sprite.play("idle")

	if velocity.x != 0:
		goes_left = velocity.x < 0
		var cat_position = cat_sprite.position.x
		cat_sprite.flip_h = goes_left
		sword.attack_left = goes_left
		
		
		sword_sprite.position.x = cat_position + 30
		sword_shape.position.x = cat_position + 30
		if goes_left:
			sword_sprite.position.x = cat_position - 30
			sword_shape.position.x = cat_position - 30

func _on_hitbox_body_entered(body):
	if body.is_in_group("enemies"):
		body.can_attack = true
		
func _on_hitbox_body_exited(body):
	if body.is_in_group("enemies"):
		body.can_attack = false

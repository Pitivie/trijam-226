extends CharacterBody2D

@onready var rat_sprite = $RatCollisionShape/RatSprite
@onready var animation_player = $AnimationPlayer
@onready var squawk_sound = $Squawk

signal killed
signal bit
signal targeted

const speed = 400
var target_body: CharacterBody2D
var can_attack = false
var noise = Vector2(randf_range(-1, 1), randf_range(-1, 1))

func _physics_process(_delta):
	velocity = position.direction_to(target_body.position + noise) * speed
	if position.distance_to(target_body.position) > 10:
		move_and_slide()

	if velocity.x != 0 || velocity.y != 0:
		rat_sprite.flip_h = velocity.x > 0

func set_target(player):
	target_body = player

func hurt():
	rat_sprite.flip_v = true
	squawk_sound.play()
	emit_signal("killed")

func _on_attack_timer_timeout():
	if can_attack:
		animation_player.play("attack")
		emit_signal("bit", 10)

func _on_squawk_finished():
	queue_free()

func _on_selection_zone_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("click"):
		emit_signal("targeted", self)

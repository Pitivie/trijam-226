extends CanvasLayer
signal new_game_requested

var playerName: String

func setPlayerName(givenName: String):
	playerName = givenName

func _on_play_again_pressed():
	emit_signal("new_game_requested", playerName)
